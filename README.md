# Keeper
Simple script in Python to run commands and watch for daemon running.
Suitable for barebone WMs (like BSPWM).

### Required:
- pgrep/pkill

### ToDo
##### [Issue #1](https://gitlab.com/fsfg/keeper/-/issues/1)
