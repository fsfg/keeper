#!/bin/sh

set -ex

export LANG=C.UTF-8
export LC_ALL=C.UTF-8
export PYTHONUNBUFFERED=1

pip install --upgrade pip poetry && poetry install
