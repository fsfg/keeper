#!/usr/bin/env python3

from configparser import ConfigParser
from sys import exit
from time import sleep
import os
import subprocess


class Keeper(object):
    """docstring for Keeper"""

    def __init__(self, commands=None, services=None):
        self.params = self._parce_params()

    def _do_sample_config(self, config_file):
        template = '# keeper.py sample config file\n\
            # `command_to_check` = `full_command_with_params`\n\
            # Ex.\n\
            # redshift = redshift -P -c ~/.config/redshift/redshift.conf\n\n\
            [runonce]\n\n\n\
            [services]'
        with open(config_file, 'w') as f:
            for line in template.split('\n'):
                f.write(f'{line.strip()}\n')

    def _parce_params(self):
        fname = os.path.basename(__file__)
        config_fname = f'{fname}.ini'

        xdg_config_home = os.getenv('XDG_CONFIG_HOME',
                                    f'/home/{os.getenv("USER")}/.config')
        config_basedir = os.path.join(xdg_config_home, fname)
        if not os.path.exists(config_basedir):
            os.makedirs(config_basedir)
        config_file = os.path.join(config_basedir, config_fname)
        _config = ConfigParser()
        _config.optionxform = str
        try:
            with open(config_file, 'r') as f:
                _config.read_file(f)
        except IOError:
            self._do_sample_config(config_file)

        config = {}
        for section in _config.sections():
            config[section] = {}
            for option in _config.options(section):
                config[section].update({option: _config.get(section, option)})

        return config

    def run_forever(self):
        _services = self.params.get('services', {})
        if not _services:
            return
        while True:
            for key, value in _services.items():
                out = subprocess.run(
                    f'pgrep --full {key}',
                    capture_output=True,
                    shell=True
                    )
                if not out.stdout:
                    try:
                        _run = subprocess.run(
                            f'{value} &',
                            shell=True,
                            timeout=3
                            )
                        if _run.returncode != 0:
                            print(f'command \'{value}\' failed')
                    except subprocess.TimeoutExpired as e:
                        print(e)
                        continue
            sleep(5)

    def run_once(self):
        _commands = self.params.get('runonce', {})
        for _, cmd in _commands.items():
            try:
                subprocess.run(
                    f'{cmd} &',
                    shell=True,
                    timeout=3)
            except Exception as e:
                print(e)

    def stop_services(self):
        print('')
        for cmd, _ in self.params.get('services').items():
            subprocess.run(['pkill', '--full', cmd])


if __name__ == '__main__':
    keeper = Keeper()

    try:
        keeper.run_once()
        keeper.run_forever()
    except KeyboardInterrupt:
        keeper.stop_services()
        print(f'{os.path.basename(__file__)} has been stopped')
        exit(0)
